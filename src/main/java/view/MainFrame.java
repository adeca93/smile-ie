package view;

import javax.swing.*;

/**
 * Created by Andrea De Castri on 20/10/2017.
 *
 */
public class MainFrame extends JFrame {

    public MainFrame(){
        super();
        this.setTitle("Training Smile");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(400, 400);
        this.setVisible(true);
    }

}
