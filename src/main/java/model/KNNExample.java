package model;

import smile.classification.KNN;
import smile.validation.CrossValidation;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by Andrea De Castri on 20/10/2017.
 *
 * K-Nearest Neighbours Example
 *
 */
public class KNNExample {

    private static final String PATH = "src/data.csv";

    private List<List<Double>> testData;
    private List<Integer> classifier;

    private double[][] data;
    private int[] clazzifier;

    public KNNExample(){
        super();
        this.testData = new ArrayList<>();
        this.classifier = new ArrayList<>();

        this.readData();
        this.validateData();
    }

    private void readData() {
        File file = new File(PATH);

        try {
            Scanner scanner = new Scanner(file);
            if(scanner.hasNextLine()){
                scanner.nextLine();
            }

            while (scanner.hasNextLine()){
                String newLine = scanner.nextLine();
                String[] line = newLine.split(",");
                List<Double> coordinate = new ArrayList<>();
                coordinate.add(Double.parseDouble(line[0]));
                coordinate.add(Double.parseDouble(line[1]));
                this.classifier.add(Integer.parseInt(line[2]));
                this.testData.add(coordinate);
            }

            this.data = new double[this.testData.size()][this.testData.get(0).size()];
            this.clazzifier = new int[this.testData.size()];
            for(int i=0; i<this.testData.size(); i++){
                for(int j=0; j<this.testData.get(0).size(); j++){
                    this.data[i][j] = this.testData.get(i).get(j);
                }
                this.clazzifier[i] = this.classifier.get(i);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void validateData(){
        int rounds = 2;

        CrossValidation crossValidation = new CrossValidation(this.data.length, rounds);

        System.out.println("RECORDS " + this.data.length);
        System.out.println("TRAIN " + crossValidation.train[0].length);

        int numTrainingRecords = crossValidation.train[0].length;
        int numTestingRecords = crossValidation.test[0].length;

        System.out.println("TRAINING PATTERN " + numTrainingRecords);

        // Per ogni round
        for(int round=0; round<crossValidation.k; round++){

            double[][] trainingData = new double[numTrainingRecords][this.data[0].length];
            int[] trainingClass = new int[numTrainingRecords];

            double[][] testingData = new double[numTestingRecords][this.data[0].length];
            int[] testingClass = new int[numTestingRecords];

            // Creazione dei record per il training
            // TODO da rivedere, forse controlli inutili, poichè la librearia divide automaticamente i gruppi di dimensioni corrette
            for(int r=0, index=0; r<rounds && index<numTrainingRecords; r++){
                if(r != round){
                    for(int k=0; k<numTrainingRecords; k++){
                        trainingData[index] = this.data[crossValidation.train[r][k]];
                        trainingClass[index] = this.clazzifier[crossValidation.train[r][k]];
                        index++;
                    }
                }
            }

            // Creazione dei record per il test
            for(int t=0; t<numTestingRecords; t++){
                testingData[t] = this.data[crossValidation.test[round][t]];
                testingClass[t] = this.clazzifier[crossValidation.test[round][t]];
            }

            KNN<double[]> knn = KNN.learn(trainingData, trainingClass, 3);

            double errors = 0;
            for(int k=0; k<testingData.length; k++){
                int prediction = knn.predict(testingData[k]);
                if(prediction != testingClass[k]){
                    errors++;
                }
            }

            System.out.println("Percentage error: " + errors / numTestingRecords * 100);

        }
    }

    public double[][] getData(){
        return this.data;
    }

    public int[] getClassifier(){
        return this.clazzifier;
    }

}
