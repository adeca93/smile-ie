package view;

import smile.plot.PlotCanvas;
import smile.plot.ScatterPlot;

import javax.swing.JPanel;
import java.awt.*;

/**
 * Created by Andrea on 20/10/2017.
 *
 */
public class KNNPanel extends JPanel {

    private ScatterPlot plot;

    public KNNPanel(double[][] data, int[] classifier){
        super();

        this.plot = new ScatterPlot(data, classifier, new Color[]{Color.BLUE, Color.RED});
    }


}
