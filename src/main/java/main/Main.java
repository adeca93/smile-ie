package main;

import model.KNNExample;
import smile.plot.PlotCanvas;
import smile.plot.ScatterPlot;
import view.MainFrame;

import java.awt.*;

/**
 * Created by Andrea De Castri on 20/10/2017.
 *
 *
 *
 */
public class Main {

    public static void main(String[] args){

        KNNExample example = new KNNExample();
        double[][] data = example.getData();
        int[] classifier = example.getClassifier();

        PlotCanvas panel = ScatterPlot.plot(data, classifier, null, new Color[]{Color.RED, Color.BLUE});

        MainFrame frame = new MainFrame();
        frame.add(panel);
    }

}
